<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HalamanUtamaController;
use App\Http\Controllers\LayananController;
use App\Http\Controllers\DetailsLayananController;
use App\Http\Controllers\ObjekWisataController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::resource('posts', PostController::class);

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', [HalamanUtamaController::class, 'dashboard'])->middleware('auth');
Route::get('logout', [AuthController::class, 'logout']);
Route::get('/', [HalamanUtamaController::class, 'index']);
Route::get('contact', [HalamanUtamaController::class, 'contact']);

// halaman tentang
Route::get('/sejarah', [HalamanUtamaController::class, 'sejarah']);
Route::get('/arti-lambang', [HalamanUtamaController::class, 'arti_lambang']);
// Route::get('layouts', [HalamanUtamaController::class, 'index_layout']);

// halaman layanan
Route::get('layanans/{id}/{layanan}', [HalamanUtamaController::class, 'index_layanan']);
Route::get('layanans/{id}/{layanan}/{slug}', [HalamanUtamaController::class, 'show_details_layanan']);
Route::resource('layanan', LayananController::class)->middleware('auth');
Route::resource('details-layanan', DetailsLayananController::class)->middleware('auth');
// Route::resource('objek-wisata', ObjekWisataController::class);

Auth::routes();


// Route::get('layanan/keamanan ', [HalamanUtamaController::class, 'index_keamanan']);
// Route::get('layanan/keamanan/{title} ', [HalamanUtamaController::class, 'show_keamanan']);
   
//halaman dashboard
// Route::get('/kesehatan/create', [HalamanUtamaController::class, 'create'])->middleware('password.confirm');

// Route::resource('keamanan', KeamananController::class)->middleware('auth');
// Route::resource('kesehatan', KesehatanController::class)->middleware('auth');
// Route::resource('transport', TransportController::class)->middleware('auth');
// Route::resource('pemadam-kebakaran',Pemadam_kebakaranController::class)->middleware('auth');
// Route::resource('lingkungan-hidup', Lingkungan_hidupController::class)->middleware('auth');
// Route::resource('olahraga', OlahragaController::class)->middleware('auth');


// Route::get('upload-image', [UploadImageController::class, 'index']);
// Route::post('save', [UploadImageController::class, 'save']);


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
