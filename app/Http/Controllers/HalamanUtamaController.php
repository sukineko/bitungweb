<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Str;
use App\Models\Kesehatan;
use App\Models\Keamanan;
use App\Models\Olahraga;
use App\Models\Transport;
use App\Models\Pemadam_kebakaran;
use App\Models\Lingkungan_hidup;
use App\Models\Layanan;
use App\Models\DetailsLayanan;



class HalamanUtamaController extends Controller
{

    public function index()
    {
        // $Layanans = Layanan::get();
        $Layanans = Layanan::with('detailsLayanan')->paginate(6);
        $Layananz = Layanan::get();
        // $slug = Str::slug($Layanans->nama, '-');

        return view("home.index", compact('Layanans','Layananz'));
    }

    public function dashboard()
    {
        $Layanans = Layanan::with('detailsLayanan')->get();
     
        // dd($Layanans);

        // $Layanann = $Layanans->detailsLayanan->

        // $Layananz = $Layanans->nama;
        // $count = Layanan::with('detailsLayanan')->where('nama',$Layananz)->count();
        // dd($layananz);
        // $keamanans = Keamanan::all()->count();
        // $olahraga = Olahraga::all()->count();
        // $transport = Transport::all()->count();
        // $pemadam_kebakaran = Pemadam_kebakaran::all()->count();
        // $lingkungan_hidup = Lingkungan_hidup::all()->count();

        return view("layananDashboard.index",compact('Layanans'));
    }

    public function sejarah()
    {
        $Layananz = Layanan::get();
        return view("about.sejarah",compact('Layananz'));
    }
        
    public function arti_lambang()
    {
        $Layananz = Layanan::get();
        return view("about.arti_lambang",compact('Layananz'));
    }

    public function contact()
    {
        $Layananz = Layanan::get();
        return view("contact.index",compact('Layananz'));
    }

    public function index_layanan($id)
    {
        /// mengambil data terakhir dan pagination 5 list
        // $Kesehatans = DetailsLayanan::with("layanan")->get()->where('slug',$layanan);\
        // Layanan::with("detailsLayanan")->get();
        $Detailz = DetailsLayanan::with('layanan')->where('layanan_id',$id)->first();
        $Details = DetailsLayanan::with('layanan')->where('layanan_id',$id)->paginate(6);
        $Layananz = Layanan::get();
        // foreach ($Details as $detail)
        // $slug = $Details->title;
        // dd($slug);

        // endforeach

        
        // $Kesehatans = Layanan::with("detailsLayanan")->where('slug',$layanan)->get();

        // dd($Details);
        /// mengirimkan variabel $Kesehatans ke halaman views Kesehatans/index.blade.php
        /// include dengan number index
        return view('Layanan.index', compact('Details','Detailz','Layananz'));
            // ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    public function show_details_layanan($id)
    {
        /// dengan menggunakan resource, kita bisa memanfaatkan model sebagai parameter
        /// berdasarkan id yang dipilih
        /// href="{{ route('Kesehatans.show',$post->id) }}
        $Details = DetailsLayanan::with('layanan')->where('layanan_id',$id)->first();
        $Layananz = Layanan::get();
        // dd($Details);
        return view('Layanan.show',compact('Details','Layananz'));
    }
}
